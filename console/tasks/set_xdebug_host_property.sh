#!/usr/bin/env bash
set -euo pipefail

XDEBUG_REMOTE_HOST=""
if [ "${MACHINE}" == "linux" ]; then
    if grep -q Microsoft /proc/version; then # WSL
        XDEBUG_REMOTE_HOST=10.0.75.1
    else
        if [ "$(command -v ip)" ]; then
            XDEBUG_REMOTE_HOST=$(ip addr show docker0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
        else
            XDEBUG_REMOTE_HOST=$(ifconfig docker0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
        fi
    fi
fi

export XDEBUG_REMOTE_HOST="${XDEBUG_REMOTE_HOST}"
