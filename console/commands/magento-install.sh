#! /usr/bin/env bash
set -euo pipefail

ARGS=( \
  "--db-host=${MYSQL_HOST:-mysql}" \
  "--db-name=${MYSQL_DATABASE:-magento}" \
  "--db-user=${MYSQL_USER:-magento}" \
  "--db-password=${MYSQL_PASSWORD:-magento}" \
  "--base-url=${MAGENTO_BASE_URL:-http://localhost}" \
  "--admin-firstname=${MAGENTO_ADMIN_FIRSTNAME:-Master}" \
  "--admin-lastname=${MAGENTO_ADMIN_LASTNAME:-Admin}" \
  "--admin-email=${MAGENTO_ADMIN_EMAIL:-foo@example.com}" \
  "--admin-user=${MAGENTO_ADMIN_USER:-admin}" \
  "--admin-password=${MAGENTO_ADMIN_PASSWORD:-admin123}" \
  "--backend-frontname=${MAGENTO_BACKEND_FRONTNAME:-admin}" \
  "--language=${MAGENTO_LANGUAGE:-en_US}" \
  "--currency=${MAGENTO_CURRENCY:-USD}" \
  "--timezone=${MAGENTO_TIMEZONE:-UTC}" \
  "--use-rewrites=1" \
)

if [[ "${MAGENTO_USE_SAMPLE_DATA:-false}" == "true" ]]; then
    ARGS+=("--use-sample-data")
fi

${COMMANDS_DIR}/magento.sh setup:install "${ARGS[@]}"