#!/usr/bin/env bash
set -euo pipefail

${COMMANDS_DIR}/exec.sh /usr/local/bin/magedbm2 "$@"
