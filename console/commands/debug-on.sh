#!/usr/bin/env bash
set -euo pipefail

${TASKS_DIR}/start_service_if_not_running.sh ${SERVICE_PHP}

f="$( ${COMMANDS_DIR}/exec.sh --root bash -c '[[ -f /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini ]] && echo "1"' )"
if [[ ! -z "${f}" ]]; then
    XDEBUG_INI_FILE='/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini'
else
    XDEBUG_INI_FILE='/usr/local/etc/php/conf.d/xdebug.ini'
fi

# read all environment variables with "XDEBUG_" prefix and set corresponding xdebug config
# for simplicity, we just append the new settings into the xdebug config file (as they override old ones).
xdebug_options=""
while IFS='=' read -r -d '' key value; do
    if [[ ${key} == XDEBUG_* ]]; then
        key="$( echo "${key}" | sed "s/XDEBUG_//" | tr "[:upper:]" "[:lower:]" )"
        xdebug_options="${xdebug_options}\nxdebug.${key}=${value}"
    fi
done < <(printenv -0)

if [[ ${xdebug_options} != "" ]]; then
    printf "${CYAN}Applying xdebug_options from environment variables...${COLOR_RESET}\n"
    ${COMMANDS_DIR}/exec.sh --root bash -c "echo -e '${xdebug_options}' >> ${XDEBUG_INI_FILE}"
fi

if [[ "${MACHINE}" == 'linux' && "${XDEBUG_REMOTE_HOST:-}" == "" ]]; then
    source ${TASKS_DIR}/set_xdebug_host_property.sh
fi

if [[ "${XDEBUG_REMOTE_HOST:-}" != "" ]]; then
    ${COMMANDS_DIR}/exec.sh --root sed -i "s/xdebug\.remote_host\=.*/xdebug\.remote_host\=${XDEBUG_REMOTE_HOST}/g" ${XDEBUG_INI_FILE}
fi

# enable extension
${COMMANDS_DIR}/exec.sh sed -i -e 's/^\;zend_extension/zend_extension/g' ${XDEBUG_INI_FILE}

if [[ "${MACHINE}" == "mac" || "${MACHINE}" == "windows" ]]; then
    printf "${YELLOW}Copying generated code into host ${COLOR_RESET}\n"
    ${COMMANDS_DIR}/mirror-container.sh -f ${GENERATED_DIR}
    # No need to restart because mirror-container.sh already does a restart
    # ${COMMANDS_DIR}/restart.sh
else
    ${COMMANDS_DIR}/restart.sh ${SERVICE_PHP}
fi

printf "${YELLOW}xdebug configuration: ${COLOR_RESET}\n"
printf "${YELLOW}--------------------------------${COLOR_RESET}\n"
${COMMANDS_DIR}/exec.sh php -i | grep -e "xdebug.idekey" -e "xdebug.remote_host" -e "xdebug.remote_port" | cut -d= -f1-2
printf "${YELLOW}--------------------------------${COLOR_RESET}\n"
